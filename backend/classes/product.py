from flask import json
from random import randint

class Product:
    def __init__(self, id:int):
        self.id = id
        self.name = f'name{id}'
        self.description = 'desc'
        self.count = 1
        self.calories = 1
        self.price = 100
        self.img_url = 'https://ic.pics.livejournal.com/pereverstal/78513478/465909/465909_900.jpg'

    def jsonify(self):
        return json.dumps(self.__dict__)

    def randomProduct():
        p = Product(randint(1, 5))
        if randint(0,100) % 2 == 0:
            p.name = f'asdfasdfasdfasdfasdfasdfasdfasdfasdfafasfsadf{p.id}'
        return p

class ProductEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Product):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)
    
    
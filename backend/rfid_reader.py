#from flask import app
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
from time import sleep
import json
from classes.product import Product, ProductEncoder
import requests
import __main__
#from app import emit_new_product


SESSION = requests.Session()

global global_product
global_product = ''

# может быть вынесено в отдельный класс Config
'''
API
'''
API = 'http://192.168.43.43:5001/'
#API = 'http://localhost:5001/'
API_AUTH_KEY = 'login'
API_TOKEN = 'login/getToken?key='
API_BUSKET = 'busket/'
API_PRODUCT = 'product/'

def init_rfid_reader(socketIo_t):
    global global_product
    global_product = ''
    print("[INFO]: Initializing rfid reader")
    reader = SimpleMFRC522()
    print(f'[INFO]: opened socket memory location: {socketIo_t}')
    print(f'[INFO]: opened socket from app memory location: {__main__.socketIo}')
    while(True):
        # add while loop to that things
        try:
            id, text = reader.read()
            print(f'[INFO]: RFID_ID: {id}')
            product = fetch_product(str(id))
            global_product = json.dumps(product)
            #__main__.socketIo.emit('getProduct', json.dumps(product))
            #socketIo_t.emit('getProduct', json.dumps(product))
            #__main__.emit_new_product(product)
        except Exception as ex:
            print(f'[ERROR]: Error occured while trying to read from RFID!\n {ex}')
        sleep(1.5)
    GPIO.cleanup()

def get_readed_product():
    return global_product

def clear_product():
    global global_product
    global_product = ''

def set_global_product(product):
    global global_product
    global_product = product

def init_rfid_reader_dummy(socketIo):
    print("[INFO]: Initializing rfid reader")
    openedSocket = socketIo
    print(f'[INFO]: opened socket memory location: {openedSocket}')
    while(True):
        # add while loop to that things
        try:
            id = '111393549263'
            print(f'[INFO]: RFID_ID: {id}')
            product = fetch_product(str(id))
            #openedSocket.emit('test', )
            sleep(5)
        except:
            print('[ERROR]: Error occured while trying to read from RFID!')

def fetch_product(id):
    r = SESSION.get(f'{API}{API_PRODUCT}getById?id={str(id)}', headers=None)
    fetched_product = json.loads(r.content.decode('utf-8'))['serialize']
    print(f'Product from DB: {fetched_product}')
    return fetched_product

# if __name__ == '__main__':
#     while True:
#         print(f'{API}{API_PRODUCT}{1}')
#         n = input('Input:')
#         fetch_product('111393549263')
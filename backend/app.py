from os import confstr, error, name
from flask import Flask, render_template, request, session
from flask_socketio import SocketIO, send, emit, join_room, leave_room, rooms
from flask_cors import CORS, cross_origin
from classes.product import Product, ProductEncoder
from flask import jsonify
from random import randint, randrange
from rfid_reader import fetch_product, init_rfid_reader, init_rfid_reader_dummy, clear_product, get_readed_product
#from rfid_reader import init_rfid_reader, init_rfid_reader_dummy
import requests
import json
import threading
import string
import subprocess
import random

product_storage = []

app = Flask('bucket')

# надо генерировать при запуске приложения
app.config['SECRET_KEY'] = ''.join(random.SystemRandom().choice(
    string.ascii_uppercase + string.digits) for _ in range(36))
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app, ressources={r"/*": {"origins": "*"}})
socketIo = SocketIO(app, cors_allowed_origins="*")
app.host = 'localhost'
local_client = None
DEFAULT_ROOM = 'bucket_room'
TEST_PRODUCT = Product(1)

AUTH_TOKEN = None
BUCKET_TOKEN = None

headers = None
SESSION = requests.Session()

'''
API
'''
API = 'http://192.168.43.43:5001/'
API_AUTH_KEY = 'login'
API_TOKEN = 'login/getToken?key='
API_BUSKET = 'busket/'
API_PRODUCT = 'product/'
'''
    Получаем информации о корзине
'''
# f = open('/proc/cpuinfo','r')
BUCKET_ID = "0123456789012345"
# а вот это уже с конфигов
BUCKET_PASS = "root"


@socketIo.on('getProduct', namespace='/')
def get_product(id):
    r = SESSION.get(f'{API}{API_PRODUCT}{randrange(1,4)}', headers=headers)
    TEST_PRODUCT = json_response(r)
    print(TEST_PRODUCT)
    return TEST_PRODUCT


@socketIo.on('getAllProducts', namespace='/')
def get_all_products():
    SESSION.get(f'{API}/busket/')


@socketIo.on('delete', namespace='/')
def delete_one(product_id):
    # socketIo.send('delete_one', room=DEFAULT_ROOM, json=product)
    socketIo.emit('delete_one', {"id": product_id})


@socketIo.on('getToken', namespace='/')
def getToken(params):
    print('TOKEN REQUESTED')
    # генерация рандомного токена (на самом деле отправляем запрос на REST)
    if BUCKET_TOKEN is not None:
        return BUCKET_TOKEN
    return 'Loading...'


@socketIo.on('test', namespace='/')
def test(params):
    print('TEST INCOME WITH PARAMS', params)
    socketIo.emit('getProduct', json.dumps(fetch_product(params)))

@socketIo.event
def connect():
    print('connected')
    # global rfid_thread
    # rfid_thread = threading.Thread(target=init_rfid_reader, args=(socketIo, ))
    # rfid_thread.daemon = True
    # rfid_thread.start()

# @socketIo.on('connect')
# def on_connect():
#     print(f'PRILETEL CHELIK {request}')
#     join_room(DEFAULT_ROOM)
@socketIo.on('GP', namespace='/')
def GP():
    print('INCOMING GP')
    print('READED PRODUCT', get_readed_product())
    socketIo.emit('getProduct', get_readed_product())
    clear_product()

@socketIo.on('join', namespace='/')
def on_join():
    join_room(DEFAULT_ROOM)
    print(f'ROOMS {rooms()}')
    #DEFAULT_ROOM = rooms()[0]

# для тестов работоспособности


@app.route('/')
def hello_world():
    return 'Hello world'


'''
Регистрируем корзину в системе на ответ получаем токен
'''


def register_bucket():
    r = SESSION.get(f'{API}{API_AUTH_KEY}?id=12&password=eah', data={
                    "id": 12, "password": "eah"}, auth=(BUCKET_ID, BUCKET_PASS))
    print(r.content)
    tokens = json_response(r)
    return tokens['mobileToken'], tokens['token']


def json_response(response):
    return json.loads(response.content.decode('utf-8'))


def main():
    BUCKET_TOKEN = 'EMPTY'
    AUTH_TOKEN = 'EMPTY'
    # https
    # socketIo.run(app, ssl_context=('cert.pem', 'key.pem'))
    # http
    # BUCKET_TOKEN, AUTH_TOKEN = register_bucket()
    headers = {"Authorization": AUTH_TOKEN}
    print(f'THIS IS BUCKET TOKEN: {BUCKET_TOKEN} {AUTH_TOKEN}')

    rfid_thread = threading.Thread(target=init_rfid_reader, args=(socketIo, ))
    rfid_thread.daemon = True
    rfid_thread.start()

    print(f'[INFO]: Running Flask APP')
    socketIo.run(app, port=5000)

def emit_new_product(product):
    print('emit_new_prodcut')
    #socketIo.emit('getProduct', json.dumps(product))
    with app.test_request_context('/'):
        socketIo.emit('getProduct', json.dumps(product), namespace='/')
    

# метод для верификации подписи исполняемого файлаыы
# gpg --gen-key
# gpg --sign <file>
# gpg --verify <file>
def verify_signature():
    try:
        cmdList = "gpg --verify bucket.gpg".split(' ')
        proc = subprocess.Popen(cmdList, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output, error = proc.communicate()
        if error:
            print("Cannot verify signature of file")
            return False
        else:
            if output.decode('utf-8').find('Good signature') > 0:
                return True
            else:
                print("Signature is not matched")
                return False
    except FileNotFoundError:
        print("Signature file not found")
        return False


if __name__ == '__main__':
    # if verify_signature():
    #     main()
    main()
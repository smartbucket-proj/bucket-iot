import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom"
import { Button, Container, Typography } from '@material-ui/core';

import ProductCard from "../components/ProductComponent";
import Product from "../classes/Product";

const BucketPage = (props) => {
	console.log('BUCKET PROPS', props.socket._callbacks.$delete_one);

	if(props.socket._callbacks.$delete_one !== undefined) {
		props.socket._callbacks.$delete_one = props.socket._callbacks.$delete_one.slice(0,0);
	}
	const [products, setProducts] = useState([]);

	const onAddProductClick = () => {
		props.socket.emit('getProduct', 1, (productJson) => {
			const newProduct = new Product(productJson);
			var founded = products.findIndex((elem) => { return elem.id == newProduct.id; })
			if(founded >= 0) {
				products[founded].count += 1;
				setProducts([...products]);
			} else {
				setProducts([...products, newProduct]);
			}
		});
	}
	props.socket.on('delete_one', (product) => {
		console.log(product);
		const founded = products.findIndex((elem) => { return elem.id == product.id; });
		if(founded >= 0) {
			products[founded].count -= 1;
			// убираем все продукты у которых count = 0
			const filteredProducts = products.filter((value) => value.count > 0);
			setProducts([...filteredProducts]);
		}
	});
	useEffect(() => {
		// запрашивать бэк на инфу про все содержимое корзины.
		// props.socket.on('getProduct', (newProduct) => {
		// 	console.log('state', products);
		// 	setProducts([...products, newProduct]);
		// })
	}, []);

	return (
		<Container maxWidth="sm">
			<div>
				<Typography variant="h4" gutterBottom>
					Ваша корзина
				</Typography>
				{products.length > 0 && products.map(prod => (
					<ProductCard key={prod.id + prod.name} product={prod} socket={props.socket} />
				))}
			</div>
			<Button onClick={onAddProductClick} color="secondary">PRESSME</Button>
		</Container>
	);
}

export default BucketPage;
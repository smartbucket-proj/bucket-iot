import React, { useState, useReducer, useEffect } from 'react';
import { Button, Container, Typography } from '@material-ui/core';


function tokenizer(state, action) {
	state.socket.emit('getToken',1 ,(newToken) => {
		console.log('REQUESTED TOKEN:', newToken);
		state.token = newToken;
	});
	return state;
}

const TokenPage = (props) => {
	// socket.socket так как это состояние
	const [state, setToken] = useState('Loading...');

	useEffect(() => {
		props.socket.emit('getToken', 1, (newToken) => {
			console.log('REQUESTED TOKEN:', newToken);
			setToken(newToken);
		});
	}, []);

	return (
		<Container maxWidth="sm">
			<div>
			<Typography variant="h1" component="h2" gutterBottom>
				{state}
			</Typography>
			</div>
			<Button variant="contained" href="/bucket">Отмена</Button>
		</Container>
	);
}

export default TokenPage;
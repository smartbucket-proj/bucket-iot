import React, { useState, useReducer } from 'react';
import { Button, Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
	yesbtn: {
		height: 50,
		width: 60,
		padding: '0 30px',
		margin: 8,
		backgroundColor: '#006400'
	},
	nobtn: {
		height: 50,
		width: 60,
		padding: '0 30px',
		margin: 8,
	}
});

const ChoosePage = (props) => {
	const [socket, dispatcher] = useReducer(null, props.socket);
	const classes = useStyles();
	return (
		<Container maxWidth="sm">
			<div>
				<Typography variant="h4" gutterBottom>
					Хотите ли вы использовать телефон для дополнительной информации о покупках?
				</Typography>
				<Button className={classes.yesbtn} variant="contained" color="primary" href="/token">Да</Button>
				<Button className={classes.nobtn} variant="contained" href="/bucket">Нет</Button>
			</div>
		</Container>
	);
}

export default ChoosePage;
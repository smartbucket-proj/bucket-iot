import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import io from "socket.io-client"


// http
const endPoint = "http://127.0.0.1:5000/";

// https
//const endPoint = "https://127.0.0.1:5000/";

const socket = io(`${endPoint}`);
socket.emit('join');
//socket.connect();

ReactDOM.render(
  <React.StrictMode>
    <App socket={ socket }/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

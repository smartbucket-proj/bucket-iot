import React from 'react';

class Product { 
    constructor(jsonString) {
        this.id = jsonString.id;
        this.name = jsonString.name;
        this.description = jsonString.description;
        //this.count = jsonString.count;
        this.count = 1;
        this.calories = jsonString.calorie;
        this.price = jsonString.price;
        this.img_url = jsonString.imageLink;
    }
}

export default Product;
import React, { useReducer } from 'react';
import './App.css';


import { BrowserRouter as Router, Route, Switch } from "react-router-dom"; 

//Importing Pages
import ChoosePage from "./pages/start"
import TockenPage from "./pages/token"
import BucketPage from "./pages/bucket"

function App(props) {
  const [state, dispatcher] = useReducer(null, {socket: props.socket});
  console.log(state);
  let socket = state.socket;
  return (
    <Router>
      <Switch>
        <Route exact path="/" render={() => <ChoosePage socket={ socket }/>}></Route>
        <Route exact path="/token" render={() => <TockenPage socket={ socket }/>}></Route>
        <Route exact path="/bucket" render={() => <BucketPage socket={ socket }/>}></Route>
      </Switch>
    </Router>
  );
}

export default App;

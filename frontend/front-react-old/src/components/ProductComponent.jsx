import React from 'react';
import { Card, CardHeader, CardMedia, CardContent,Typography,IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';


const ProductCard = (props) => {
	const product = props.product;
	const socket = props.socket;
	
	const onDeletePress = () => {
		socket.emit('delete', product.id);
	};
	console.log(product);
	if(product !== undefined) {
		return (
			<Card style={{maxWidth: 345}} id={product.id.toString()} variant="outlined">
				<CardHeader title={`${product.id} ${product.name}`} action={
					<IconButton onClick={onDeletePress} aria-label="settings">
						<DeleteIcon />
					</IconButton>
				}>
				</CardHeader>
				<CardMedia style={{height:0, paddingTop: '56.25%'}} title={product.name.toString()} image={product.img_url.toString()}>
				</CardMedia>
				<CardContent>
					<Typography variant="body2" color="textSecondary" component="p">
						{product.description}
					</Typography>
					<Typography variant="h5" color="textPrimary" component="p">
						{`Цена:${product.price} Калории:${product.calories} Кол-во: ${product.count}`}
					</Typography>
				</CardContent>
			</Card>
		); 
	}

} 

export default ProductCard;

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { StartPageComponent } from '../app/components/start-page/start-page.component';
import { ProductListPageComponent } from '../app/components/product-list-page/product-list-page.component';

const routes: Routes = [
  {path : '', component : StartPageComponent},
  {path: 'list', component: ProductListPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

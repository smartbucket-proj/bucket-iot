import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Product } from '../models/Product';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private socket: Socket) {
    this.test(5);
    this.socket.on('test', (sendedMsg) => {
      console.log(sendedMsg);
    });
  }

  test(id: number) {
    this.socket.emit('test', id);
  }

  getProduct(id: number) {
    this.socket.emit('getProduct', id);
  }
}

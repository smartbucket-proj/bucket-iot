import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { Product } from '../../models/Product';


@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {
  constructor(private socket: SocketService) { }

  tmp: Product;

  ngOnInit(): void {
    this.tmp = new Product(null);
    this.tmp.id = '1';
    this.tmp.name = 'Name';
    this.tmp.description = 'Descirption';
    this.tmp.imgUrl = 'https://fruktoviysad.ru/uploads/media/product/0001/16/a27ca7a86c649531c94e3608d77e4de27566f2bd.png'
    this.tmp.count = 1;
    this.tmp.price = 100;
    this.tmp.calories = 100;
  }
}

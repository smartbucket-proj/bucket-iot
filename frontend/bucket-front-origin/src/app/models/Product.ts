export class Product {

    id: string;
    name: string;
    description: string;
    count: number;
    calories: number;
    price: number;
    imgUrl: string;

    constructor(json) {
        if (json) {
            this.id = json.id;
            this.name = json.name;
            this.description = json.description;
            //this.count = json.count;
            this.count = 1;
            this.calories = json.calorie;
            this.price = json.price;
            this.imgUrl = json.imageLink;
        }

    }
}

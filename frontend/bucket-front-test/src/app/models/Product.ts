export class Product {

    id: string;
    name: string;
    description: string;
    count: number;
    calories: number;
    price: number;
    imgUrl: string;

    // Поля необходимо указывать как в приходящем json
    constructor(json) {
        if (json) {
            this.id = json.id;
            this.name = json.name;
            this.description = json.description;
            this.count = json.count ? json.count : 1;
            this.calories = json.calories;
            this.price = json.price;
            this.imgUrl = json.img_url ? json.img_url : json.imgUrl;
        }
    }
}

import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { Product } from '../../models/Product';
import { ActivatedRoute, Router } from '@angular/router';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {
  constructor(private socket: SocketService, private router: Router, private route: ActivatedRoute) { }

  selectedProduct: Product;

  ngOnInit(): void {
    // выбранный продукт на скрине
    this.selectedProduct = new Product(JSON.parse(this.route.snapshot.paramMap.get('selectedProduct')));
  }
}

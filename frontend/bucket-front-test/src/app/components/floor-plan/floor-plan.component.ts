import { Component, OnInit, ViewEncapsulation } from '@angular/core';
//import { Socket } from 'dgram';
import * as go from 'gojs';

@Component({
  selector: 'app-floor-plan',
  templateUrl: './floor-plan.component.html',
  styleUrls: ['./floor-plan.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FloorPlanComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.initDiagram();
  }

  public initDiagram() {
    //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    let myDiagram =
      $(go.Diagram, "myDiagramDiv",
        {
          initialContentAlignment: go.Spot.TopLeft,
          isReadOnly: true,  // allow selection but not moving or copying or deleting
          "toolManager.hoverDelay": 100,  // how quickly tooltips are shown
          "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom  // mouse wheel zooms instead of scrolls
        });

    // the background image, a floor plan
    myDiagram.add(
      $(go.Part,  // this Part is not bound to any model data
        {
          layerName: "Background", position: new go.Point(0, 0),
          selectable: false, pickable: false
        },
        $(go.Picture, "https://upload.wikimedia.org/wikipedia/commons/9/9a/Sample_Floorplan.jpg")
      ));

    // the template for each kitten, for now just a colored circle
    myDiagram.nodeTemplate =
      $(go.Node,
        new go.Binding("location", "loc"),  // specified by data
        { locationSpot: go.Spot.Center },   // at center of node
        $(go.Shape, "Circle",
          { width: 12, height: 12, stroke: null },
          new go.Binding("fill", "color")),  // also specified by data
        { // this tooltip shows the name and picture of the kitten
          toolTip:
            $("ToolTip",
              $(go.Panel, "Vertical",
                $(go.Picture,
                  new go.Binding("source", "src", function (s) { return "images/" + s + ".png"; })),
                $(go.TextBlock, { margin: 3 },
                  new go.Binding("text", "key"))
              )
            )  // end Adornment
        }
      );

    // pretend there are four kittens
    myDiagram.model.nodeDataArray = [
      { key: "bucket1", src: "50x40", loc: new go.Point(220, 130), color: "blue" },
      // { key: "bucket2", src: "55x55", loc: new go.Point(420, 250), color: "green" },
      // { key: "bucket3", src: "60x90", loc: new go.Point(640, 450), color: "red" },
      // { key: "bucket4", src: "80x50", loc: new go.Point(140, 350), color: "purple" }
    ];

    // This code keeps all nodes at a constant size in the viewport,
    // by adjusting for any scaling done by zooming in or out.
    // This code ignores simple Parts;
    // Links will automatically be rerouted as Nodes change size.
    var origscale = NaN;
    myDiagram.addDiagramListener("InitialLayoutCompleted", function (e) { origscale = myDiagram.scale; });
    myDiagram.addDiagramListener("ViewportBoundsChanged", function (e) {
      if (isNaN(origscale)) return;
      var newscale = myDiagram.scale;
      if (e.subject.scale === newscale) return;  // optimization: don't scale Nodes when just scrolling/panning
      myDiagram.skipsUndoManager = true;
      myDiagram.startTransaction("scale Nodes");
      myDiagram.nodes.each(function (node) {
        node.scale = origscale / newscale;
      });
      myDiagram.commitTransaction("scale Nodes");
      myDiagram.skipsUndoManager = false;
    });

    // simulate some real-time position monitoring, once every 2 seconds
    function randomMovement() {
      var model = myDiagram.model;
      model.startTransaction("update locations");
      var arr = model.nodeDataArray;
      var picture = myDiagram.parts.first();
      for (var i = 0; i < arr.length; i++) {
        var data = arr[i];
        var pt = data.loc;
        var x = pt.x + 20 * Math.random() - 10;
        var y = pt.y + 20 * Math.random() - 10;
        // make sure the kittens stay inside the house
        var b = picture.actualBounds;
        if (x < b.x || x > b.right) x = pt.x;
        if (y < b.y || y > b.bottom) y = pt.y;
        model.setDataProperty(data, "loc", new go.Point(x, y));
      }
      model.commitTransaction("update locations");
    }
    function loop() {
      //posArr = socket.getActualPos()
      setTimeout(function () { randomMovement(); loop(); }, 3000);
    }
    loop();  // start the simulation
  }

}

import { Component, OnInit } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { Product } from '../../models/Product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list-page',
  templateUrl: './product-list-page.component.html',
  styleUrls: ['./product-list-page.component.css']
})
export class ProductListPageComponent implements OnInit {

  productList: Product[] = new Array<Product>();

  constructor(private socket: SocketService, private router: Router) { }

  ngOnInit(): void {
    this.productList = this.socket.getActualProductList();
  }

  getTestProduct() {
    this.socket.test((Math.floor(Math.random() * 2) % 2 == 0) ? 111393549263 : 784914424156);
    this.productList = this.socket.getActualProductList();
  }

  //перекидываем на экран с детализацией
  rowSelected(row:any) {
    this.router.navigate(['/current_product', { selectedProduct: JSON.stringify(this.productList[row])}]);

  }

  //переходим на карту
  goToShopFloor() {
    this.router.navigate(['/plan'])
  }

  // итоговая цена
  finalPrice() {
    return this.productList.reduce((prev, cur) => prev + cur.count * cur.price, 0);
  }

  countAll() {
    return this.productList.reduce((prev, current, index, array) => {
      return prev + current.count;
    }, 0);
  }
  
}

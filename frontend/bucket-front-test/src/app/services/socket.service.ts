import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { Injectable } from '@angular/core';
import { send } from 'process';
import * as io from 'socket.io-client';
import { Product } from '../models/Product';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket: any;
  actualList: any;
  productList: Product[];

  constructor() {
    this.productList = new Array<Product>();
    this.socket = io.io('http://localhost:5000');

    console.log(`our socket: {this.socket}`);
    console.log(this.socket);
    this.socket.emit('getCurrentProducts');

    // лисенер на получение ифнормации о списке продуктов
    this.socket.on('recvCurrentProducts', (productList) => {
      console.log(productList);
    });

    // лисенер на то чтобы получать инфу о продукте
    this.socket.on('getProduct', (sendedMsg) => {
      console.log('GOT PRODUCT', sendedMsg);
      if(sendedMsg != '') {
        console.log('GOT PRODUCT');
        let tmpObj:Product = JSON.parse(sendedMsg);
        let foundedProduct = this.productList.map(p => p.id).indexOf(tmpObj.id);
        
        if(foundedProduct > -1) {
          this.productList[foundedProduct].count += 1;
        } else {
          this.productList.push(new Product(tmpObj));
        }
      }
    });

    this.socket.on('recvActualPos', (sendedMsg) => {
      console.log('GOT POS');
      let tmpPos = JSON.parse(sendedMsg);

      this.actualList.x = tmpPos.x;
      this.actualList.y = tmpPos.y;
    });
    this.activate_loop(this.socket);
  }

  public activate_loop(socket) {
    function loop() {
      setTimeout(function () { 
        console.log('enter loop');
        console.log(socket);
        if(socket) {
          socket.emit('GP');
        }
        loop(); 
      }, 7000);
    }
    loop();
  }
 

  test(id: number) {
    this.socket.emit('test', id);
  }

  /**
   * 
   * @param id id продекта?
   */
  getProduct(id: number) {
    this.socket.emit('getProduct', id);
  }

  /**
   * метод который возвращает текущий список продуктов
   * @returns список продуктов
   */
  getActualProductList() {
    return this.productList;
  }

  getActualPos() {
    this.socket.emit('getActualPos')
    return 
  }
}

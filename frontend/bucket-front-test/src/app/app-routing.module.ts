import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { StartPageComponent } from '../app/components/start-page/start-page.component';
import { ProductListPageComponent } from '../app/components/product-list-page/product-list-page.component';
import { FloorPlanComponent } from '../app/components/floor-plan/floor-plan.component';

const routes: Routes = [
  {path: '', component : ProductListPageComponent},
  {path: 'current_product', component: StartPageComponent},
  {path: 'plan', component: FloorPlanComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
